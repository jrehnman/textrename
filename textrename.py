#!/bin/python

import argparse
import itertools
import os
import os.path
import subprocess
import sys
import tempfile

def confirm(text):
    while True:
        sys.stderr.write(text + ' (y/N): \n')
        response = sys.stdin.readline()
        if response.lower() == 'y\n':
            return True
        elif response.lower() == 'n\n':
            return False
        elif response == '\n':
            return False

def main():
    parser = argparse.ArgumentParser(description='Batch rename files using text editor')
    parser.add_argument('--confirm', '-c', action='store_true', help='confirm before action')
    parser.add_argument('--delete', '-d', action='store_true', help='delete files with blank names')
    parser.add_argument('--dry-run', '-n', action='store_true', help='don\'t perform the actions, implies -v')
    parser.add_argument('--editor', '-e', default=os.environ.get('EDITOR'), help='editor to use, defaults to $EDITOR')
    parser.add_argument('--overwrite', '-o', action='store_true', help='overwrite existing files')
    parser.add_argument('--silent', '-s', action='store_true', help='supress error messages')
    parser.add_argument('--verbose', '-v', action='store_true', help='print actions as they are performed')
    parser.add_argument('filename', nargs='+', help='list of files to rename')
    options = parser.parse_args()

    if not options.editor:
        if not options.silent:
            sys.stderr.write('$EDITOR not set and no editor supplied on command line\n')
        return -1

    if options.dry_run:
        options.verbose = True

    with tempfile.NamedTemporaryFile() as tmp:
        tmp.writelines([fn + '\n' for fn in options.filename])
        tmp.flush()

        exitcode = os.spawnlp(os.P_WAIT, options.editor, options.editor, os.path.realpath(tmp.name))

        tmp.seek(0)
        newfilenames = [fn.strip('\r\n') for fn in tmp.readlines()]
        for (a, b) in itertools.izip_longest(options.filename, newfilenames):
            if not b:
                if options.delete:
                    if options.verbose and not options.confirm:
                        sys.stderr.write('Deleting {}\n'.format(a))

                    if not options.dry_run:
                        if not options.confirm or confirm('Delete {}'.format(a)):
                            os.remove(a)

                else:
                    if options.verbose:
                        sys.stderr.write('Skipping {}\n'.format(a))

            elif a != b:
                if os.path.exists(b) and not options.overwrite:
                    if not options.silent:
                        sys.stderr.write('Can\'t rename {0} to {1}, {1} already exists\n'.format(a, b))

                else:
                    if options.verbose and not options.confirm:
                        sys.stderr.write('Renaming {} -> {}\n'.format(a, b))

                    if not options.dry_run:
                        if not options.confirm or confirm('Rename {} to {}'.format(a, b)):
                            os.rename(a, b)

if __name__ == '__main__':
    main()

